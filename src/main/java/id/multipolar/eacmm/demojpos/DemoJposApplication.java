package id.multipolar.eacmm.demojpos;

import org.jpos.q2.Q2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoJposApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(DemoJposApplication.class, args);
	}

	public void run(String... args) throws Exception {
		Q2 q2 = new Q2();
		Thread thread = new Thread(q2);
		q2.start();
	}
	
	

}
